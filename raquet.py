import pygame
from utils import *
from ball import Ball

class Raquet(pygame.sprite.Sprite):

    def __init__(self, difficulty, posx=0, posy=0):
        pygame.sprite.Sprite.__init__(self)
        self.images = load_images(['paleta_easy.png', 'paleta_medium.png'])
        self.image_index = 0
        self.speed = 14
        if difficulty == "EASY":
            self.image_index = 0
        elif difficulty == "MEDIUM":
            self.image_index = 1
        elif difficulty == "HARD":
            self.image_index = 1

        self.image = self.images[self.image_index]
        self.rect = self.image.get_rect()
        if posx == 0 and posy == 0:
            self.rect.centerx = SCREENRECT.width/2
            self.rect.centery = SCREENRECT.height-10
        else:
            self.rect.centerx = posx
            self.rect.centery = posy

        self.alive = True

    def move(self, scenario, hdirection, vdirection):
        if scenario.collision_solid_wall_left(self):
            self.rect.move_ip(10,0)
        elif scenario.collision_solid_wall_right(self):
            self.rect.move_ip(-10,0)
        else:
            self.rect.move_ip(hdirection*self.speed, 0)
            self.rect.move_ip(0, vdirection * self.speed)

    def draw(self, surface):
        surface.blit(self.image, self.rect)
