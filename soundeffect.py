import pygame

class Sound(object):

    def __init__(self, sound):
        # LOAD SOUND
        pygame.mixer.pre_init(54100, 16, 2, 4096)  # frequency, size, channels, buffersize
        pygame.init()  # turn all of pygame on.
        self.sound = True
        self._raquet_effect = pygame.mixer.Sound('res/raquet_touch.wav')
        self._wall_effect = pygame.mixer.Sound('res/wall_touch.wav')
        self._block_effect = pygame.mixer.Sound('res/block_touch.wav')

    def raquet_effect(self):
        if self.sound:
            self._raquet_effect.play()

    def wall_effect(self):
        if self.sound:
            self._wall_effect.play()

    def block_effect(self):
        if self.sound:
            self._block_effect.play()