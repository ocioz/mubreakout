from utils import *
import pygame

class Ball(pygame.sprite.Sprite):
    def __init__(self, posx, posy):
        pygame.sprite.Sprite.__init__(self)
        self.speed = 8
        self.image = load_image('balls/ball1.png')
        self.rect = self.image.get_rect()
        self.rect.centerx = posx
        self.rect.centery = posy
        self.hdirection = -1 # -1: left / 1: right
        self.vdirection = -1 # -1:up 1: down

    def move_up(self):
        self.vdirection = -1

    def move_down(self):
        self.vdirection = 1

    def move_left(self):
        self.hdirection = -1

    def move_right(self):
        self.hdirection = 1

    def move(self, scenario):
        if scenario.collision_solid_wall_left(self):
            self.move_right()
        elif scenario.collision_solid_wall_right(self):
            self.move_left()
        elif scenario.collision_solid_roof(self):
            self.move_down()
        elif scenario.collision_with_raquet(self):
            self.move_up()
        elif scenario.collision_block(self):
            pass
            #if self.vdirection == -1:
            #    self.move_down()
            #else:
            #    self.move_up()

        self.rect.move_ip(self.hdirection * self.speed, 0)
        self.rect.move_ip(0, self.vdirection * self.speed)


    def draw(self, surface):
        surface.blit(self.image, self.rect)
