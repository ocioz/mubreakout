
class ROBOT_IA(object):

    def __init__(self, GAME_MODE):
        self.raquet_speed = 3
        self.game_mode = GAME_MODE

    def move_raquet(self, scenario):
        if len(scenario.ball_list)==0:
            scenario.shot()
        for ball in scenario.ball_list:
            for raquet in scenario.raquet_list.sprites():
                if ball.rect.centerx >= raquet.rect.centerx+10 and ball.vdirection==1:
                    return 1
                elif ball.rect.centerx < raquet.rect.centerx-10 and ball.vdirection==1:
                    return -1
        return 0

    def toggle_IA(self):
        if self.game_mode:
            self.game_mode = False
        else:
            self.game_mode = True

    def is_working(self):
        return self.game_mode