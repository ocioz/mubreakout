import pygame
from utils import *

class Heart(pygame.sprite.Sprite):

    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.images = load_images(['heart_red.png', 'heart_blank.png'])
        self.image_index = 0
        self.image = self.images[self.image_index]
        self.rect = (x, y)

    def draw(self, surface):
        surface.blit(self.images[self.image_index], self.rect)
