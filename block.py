import pygame
from utils import *

class Block(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)

    def move_down(self):
        self.rect.move_ip(0, 36)

class RedBlock(Block):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.speed = 4
        self.images = load_images(['50x32red.png','50x32redBroken1.png', 
        						'50x32redBroken2.png', '50x32redBroken.png'])
        self.image_index = 0
        self.image = self.images[self.image_index]
        self.rect = self.image.get_rect()
        self.rect.centerx = SCREENRECT.width / 2
        self.rect.centery = 60
        self.life = 4


    def draw(self, surface):
        self.image = self.images[self.image_index]
        surface.blit(self.image, self.rect)

    def hitted(self):
        self.life -= 1
        if self.life == 3:
            self.image_index = 1
        elif self.life == 2:
            self.image_index = 2
        elif self.life == 1:
            self.image_index = 3
        elif self.life == 0:
            self.kill()

class YellowBlock(Block):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.speed = 4
        self.images = load_images(['50x32yellow.png','50x32yellowBroken1.png', '50x32yellowBroken.png'])
        self.image_index = 0
        self.image = self.images[self.image_index]
        self.rect = self.image.get_rect()
        self.rect.centerx = SCREENRECT.width / 2
        self.rect.centery = 60
        self.life = 3


    def draw(self, surface):
        self.image = self.images[self.image_index]
        surface.blit(self.image, self.rect)

    def hitted(self):
        self.life -= 1
        if self.life == 2:
            self.image_index = 1
        elif self.life == 1:
            self.image_index = 2
        elif self.life == 0:
            self.kill()

class BlueBlock(Block):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.speed = 4
        self.images = load_images(['50x32blue.png','50x32blueBroken.png'])
        self.image_index = 0
        self.image = self.images[self.image_index]
        self.rect = self.image.get_rect()
        self.rect.centerx = SCREENRECT.width / 2
        self.rect.centery = 60
        self.life = 1


    def draw(self, surface):
        if self.life == 1:
            self.image = self.images[self.image_index]
        surface.blit(self.image, self.rect)

    def hitted(self):
        self.life -= 1
        if self.life == 1:
            self.image_index = 1
        elif self.life == 0:
            self.kill()

