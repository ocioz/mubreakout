import pygame
from utils import *

class Background(pygame.sprite.Sprite):

    def __init__(self, difficulty_index=0):
        pygame.sprite.Sprite.__init__(self)
        self.difficulty = difficulty_index
        self.heading = 0 # N: 0 / E: 1 / S: 2 / W: 3
        self.x = -10
        self.y = -10
        self.image_index = self.difficulty
        self.images = load_images(['breakout_bg1.png', 'breakout_bg2.png', 'breakout_bg3.png'])
        self.background = self.images[0]

    def change_difficulty(self):
        if self.difficulty < 2:
            self.difficulty+=1
            self.image_index += 1

    def draw(self, surface):
        self.calculate_next_step()
        surface.blit(self.images[self.image_index], (self.x,self.y))

    def calculate_next_step(self):
        if self.heading == 0:
            if self.y == -10:
                self.heading = 1
            else:
                self.y -= 1
        elif self.heading == 1:
            if self.x == 0:
                self.heading = 2
            else:
                self.x +=1
        elif self.heading == 2:
            if self.y == 0:
                self.heading = 3
            else:
                self.y+=1
        elif self.heading == 3:
            if self.x == -10:
                self.heading = 0
            else:
                self.x-=1


