import pygame
from utils import *

class WallBlock(pygame.sprite.Sprite):
    def __init__(self, posx, posy):
        pygame.sprite.Sprite.__init__(self)
        self.speed = 4
        self.image = load_image('block_undestroyable.png')
        self.rect = self.image.get_rect()
        self.rect.left = posx
        self.rect.top = posy
        self.animation_time = 1

    def draw(self, surface):
        surface.blit(self.image, self.rect)
